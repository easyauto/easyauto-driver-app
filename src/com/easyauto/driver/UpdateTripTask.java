package com.easyauto.driver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class UpdateTripTask extends AsyncTask<Trip.TRIP_STATES, Integer, Boolean> {

    private static String UPDATE_URL = "http://www.findauto.in/update_trip.php";
    private static String TAG = "EADUpdateTripTask";
    
    private Context context;
    private String tripId;
    
    public UpdateTripTask(Context context, String tripId){
        this.context = context.getApplicationContext();
        this.tripId = tripId;
    }

    
    @Override
    protected Boolean doInBackground(Trip.TRIP_STATES... update) {
        
        if(update != null && update[0] != null) {
            return updateTrip(update[0]);
        }
        return false;
    }
    
    private Boolean updateTrip(Trip.TRIP_STATES updateStatus){
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(UPDATE_URL);

        try {
            // TODO: Implement basic authentication
            Log.i(TAG,"Updating trip "+tripId+ " as "+ updateStatus.toString());
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("driver_id", HomeActivity.getDriverId()+""));
            nameValuePairs.add(new BasicNameValuePair("trip_id", tripId));
            nameValuePairs.add(new BasicNameValuePair("update_status", updateStatus.toString()));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            
            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
                //TODO: Check response text for success or failure
                return true;
            }
            return false;
        } catch (ClientProtocolException e) {
            Log.i(TAG,"ClientProtocolException while updating location "+e.getMessage());
            return false;
        } catch (IOException e) {
            Log.i(TAG,"IOException while updating location "+e.getMessage());
            return false;
        }
    }
    
    @Override
    protected void onPostExecute(Boolean result) {
        if(result){
            Globals.isAvailable = true;
            Intent mIntent = new Intent(context, HomeActivity.class);
            mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(mIntent);
        }
    }

}

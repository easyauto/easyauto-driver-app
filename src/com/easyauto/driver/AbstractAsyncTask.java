package com.easyauto.driver;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

public abstract class AbstractAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
    
    private Context context;
    private ProgressDialog progress;
    
    AbstractAsyncTask(Context context){
        this.context = context;
    }

    public void onPreExecute(){
        progress =  ProgressDialog.show(context, context.getString(R.string.app_name), context.getString(R.string.performing_operation) );
    }
    
    public void onPostExecute(Result res){
        progress.dismiss();
    }
}

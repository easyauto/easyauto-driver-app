package com.easyauto.driver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

public class GeoUpdateTask extends AsyncTask<Location, Integer, Void> {
    
    private static String UPDATE_URL = "http://findauto.in/updateDriverLocation.php";
    private static String TAG = "EADGeoUpdateTask";

	@Override
	protected Void doInBackground(Location... loc) {
	    if(loc != null && loc[0] != null){
	        publishLocation(loc[0].getLatitude(), loc[0].getLongitude(), HomeActivity.getDriverId());
	    }
	    return null;
	}

	private void publishLocation(double lat, double lng, String driverId) {
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost(UPDATE_URL);

	    try {
	        // TODO: Implement basic authentication
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
	        nameValuePairs.add(new BasicNameValuePair("driver_id", driverId+""));
	        nameValuePairs.add(new BasicNameValuePair("lat", lat+""));
	        nameValuePairs.add(new BasicNameValuePair("long", lng+""));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        
	        if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
	            Log.i(TAG, "Updated location successfully with lat "+lat+" lng "+lng);
	        }
	        
	    } catch (ClientProtocolException e) {
	        Log.i(TAG,"ClientProtocolException while updating location "+e.getMessage());
	    } catch (IOException e) {
	        Log.i(TAG,"IOException while updating location "+e.getMessage());
	    }
	}

}

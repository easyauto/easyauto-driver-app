package com.easyauto.driver;

import java.util.ArrayList;

import com.easyauto.driver.Trip.TRIP_STATES;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TripDetailsActivity extends Activity {
    
    TextView fromTextView;
    Trip currentTrip;
    Button updateTripBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_trip_details);
        
        currentTrip = Globals.currentTrip;
        
        fromTextView = (TextView) findViewById(R.id.from_text);
        fromTextView.setText(String.format(getString(R.string.from), 
                currentTrip.getFromLocation()));
        
        updateTripBtn = (Button) findViewById(R.id.complete_trip_btn);
        updateTripBtn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                showTripCompletionDialog();
                
                startHomeActivity();
            }
        });
    }

    protected void showTripCompletionDialog() {
        final CharSequence[] options = getUpdateOptions();
        
        AlertDialog.Builder builder =  new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.update_trip_msg));

        builder.setSingleChoiceItems(options, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch(item)
                {
                    case 0:
                             showUpdateConfirmationDialog(Trip.TRIP_STATES.SUCCESS);
                             break;
                    case 1:
                            showUpdateConfirmationDialog(Trip.TRIP_STATES.FAILURE);
                            break;
                }
                dialog.dismiss();
                }
            });
        builder.show();

    }


    protected void showUpdateConfirmationDialog(final TRIP_STATES tripState) {
        String confirmMsg = null;
        switch(tripState){
        case SUCCESS:
            confirmMsg = getString(R.string.trip_success_verify);
            break;
        case FAILURE:
            confirmMsg = getString(R.string.trip_fail_verify);
            break;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(confirmMsg);
        builder.setTitle(this.getString(R.string.trip_details));
        builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                new UpdateTripTask(TripDetailsActivity.this, currentTrip.getId()).execute(tripState);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        builder.show();
    }

    private CharSequence[] getUpdateOptions() {
        ArrayList<CharSequence> options = new ArrayList<CharSequence>();
        options.add(getString(R.string.trip_success));
        options.add(getString(R.string.trip_fail));
        return options.toArray(new CharSequence[options.size()]);
    }

    protected void startHomeActivity() {
        // TODO Auto-generated method stub
        
    }
}

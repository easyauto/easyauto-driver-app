package com.easyauto.driver;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.internal.mc;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationListener;


public class HomeActivity extends ActionBarActivity implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

    private static String PHONE_NUMBER = "phoneNumber";
    private static String DRIVER_ID = "driverId";
    private static String DEFAULT_PREF = "0";
    
    private static int LOCATION_UPDATE_INTERVAL = 60 * 1000;  //1 min

    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    String SENDER_ID = "823253310355";

    /**
     * Tag used on log messages.
     */
    static final String TAG = "EADHomeActivity";

    Button availabilityToggleBtn;
    RelativeLayout homeLayout;
    TextView statusDescTextView;

    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    SharedPreferences prefs;
    Context context;

    private String gcmId, phoneNumber;
    private static String driverId;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        homeLayout = (RelativeLayout) findViewById(R.id.home_layout);
        statusDescTextView = (TextView) findViewById(R.id.status_desc_text_view);
        availabilityToggleBtn = (Button) findViewById(R.id.availability_toggle_btn);
        availabilityToggleBtn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                    toggleAvailability();
            }
        });

        context = getApplicationContext();
        
        prefs = getPreferences(0);
        phoneNumber = prefs.getString(PHONE_NUMBER, DEFAULT_PREF);
        if(DEFAULT_PREF.equals(phoneNumber)){
            //collect phone number
            collectPhoneNumber();
        }

        // Check device for Play Services APK. If check succeeds, proceed with
        //  GCM registration.
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            gcmId = getRegistrationId(context);
            
            Log.i(TAG, " regid is "+gcmId);
            sendRegistrationIdToBackend();
            if (gcmId.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
        
        Intent intent = getIntent();

        if(intent != null && intent.hasExtra(Trip.TRIP_FROM) &&
                Globals.isAvailable != null && Globals.isAvailable){
            //create new trip request
            Trip newTripRequest = new Trip(intent.getStringExtra(Trip.TRIP_FROM), intent.getStringExtra(Trip.TRIP_ID),
                    Double.parseDouble(intent.getStringExtra(Trip.TRIP_FROM_LAT)),
                    Double.parseDouble(intent.getStringExtra(Trip.TRIP_FROM_LNG)));

            //force update UI as activity gets refreshed
            updateUI(Globals.isAvailable);
            showTripDialog(newTripRequest);
        } else {
            //not available by default
            Globals.isAvailable = false;
        }

        buildGoogleApiClient();
        createLocationRequest();
        mGoogleApiClient.connect();
        
    }


    private void toggleAvailability() {
        //toggle availability
        Globals.isAvailable = !Globals.isAvailable;
        Log.i(TAG,"isAvailable has been changed to "+Globals.isAvailable);

        if(Globals.isAvailable){
            startLocationUpdates();
            availabilityToggleBtn.setText(R.string.mark_busy);
        } else {
            stopLocationUpdates();
            availabilityToggleBtn.setText(R.string.mark_available);
        }

        updateUI(Globals.isAvailable);
    }
    
    private void updateUI(Boolean isAvailable){
        if(isAvailable){
            statusDescTextView.setText(getString(R.string.available_info));
            homeLayout.setBackgroundColor(getResources().getColor(R.color.green));
        } else {
            statusDescTextView.setText(getString(R.string.busy_info));
            homeLayout.setBackgroundColor(getResources().getColor(R.color.white));
        }
    }

    private boolean registerDriver(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        storePhoneNumber(context, phoneNumber);
        try {
            this.driverId =  new RegisterDriverTask(context).execute(phoneNumber).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        storeDriverId(context, driverId);
        return true;
    }


    private void collectPhoneNumber() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle(getString(R.string.collect_mobile_number_title));
        alert.setMessage(getString(R.string.collect_mobile_number));

        // Set an EditText view to get user input 
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        alert.setView(input);

        alert.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int whichButton) {
            if(input.getText().toString().length() != 10 || !registerDriver(input.getText().toString())) {
              collectPhoneNumber();
            }
          }
        });

        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
              finish();
          }
        });

        alert.show();
    }


    private void showTripDialog(final Trip tripRequest) {
        if(Globals.mCurrentLocation == null){
            Log.w(TAG,"Not showing trip dialog as driver's current location has not been determined");
            return;
        }
        double distanceToCustomer = 0.85;
        //distanceBetween(tripRequest.getFromLat(), tripRequest.getFromLng(), 
        //        Globals.mCurrentLocation.getLatitude(), Globals.mCurrentLocation.getLongitude());
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(String.format(this.getString(R.string.trip_details_confirm)
                , tripRequest.getFromLocation(), distanceToCustomer+" km"));
        builder.setTitle(this.getString(R.string.trip_details));
        builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                new AcceptTripTask(HomeActivity.this).execute(tripRequest);
            }
        });
        builder.setNegativeButton(R.string.decline, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        builder.show();
    }


    private double distanceBetween(double lat1, double lng1,
            double lat2, double lng2) {
        // TODO Auto-generated method stub
        return 0;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
 // You need to do the Play Services APK check here too.
    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    
    /**
    * Gets the current registration ID for application on GCM service.
    * <p>
    * If result is empty, the app needs to register.
    *
    * @return registration ID, or empty string if there is no existing
    *         registration ID.
    */
   private String getRegistrationId(Context context) {
       final SharedPreferences prefs = getGCMPreferences(context);
       String registrationId = prefs.getString(PROPERTY_REG_ID, "");
       if (registrationId.isEmpty()) {
           Log.i(TAG, "Registration not found.");
           return "";
       }

       int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
       int currentVersion = getAppVersion(context);
       if (registeredVersion != currentVersion) {
           Log.i(TAG, "App version changed.");
           return "";
       }
       return registrationId;
   }

   /**
    * @return Application's {@code SharedPreferences}.
    */
   private SharedPreferences getGCMPreferences(Context context) {
       return getSharedPreferences(HomeActivity.class.getSimpleName(),
               Context.MODE_PRIVATE);
   }

   /**
    * @return Application's version code from the {@code PackageManager}.
    */
   private static int getAppVersion(Context context) {
       try {
           PackageInfo packageInfo = context.getPackageManager()
                   .getPackageInfo(context.getPackageName(), 0);
           return packageInfo.versionCode;
       } catch (NameNotFoundException e) {
           // should never happen
           throw new RuntimeException("Could not get package name: " + e);
       }
   }

   /**
    * Registers the application with GCM servers asynchronously.
    * <p>
    * Stores the registration ID and app versionCode in the application's
    * shared preferences.
    */
   private void registerInBackground() {
       new AsyncTask <Void,Void,String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    gcmId = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + gcmId;

                    sendRegistrationIdToBackend();

                    storeRegistrationId(context, gcmId);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.i(TAG,msg);
            }
        }.execute(null, null, null);

   }
   
   private void sendRegistrationIdToBackend() {
        new UpdateGcmIdTask(context).execute(gcmId);
    }
   
   /**
    * Stores the registration ID and app versionCode in the application's
    * {@code SharedPreferences}.
    *
    * @param context application's context.
    * @param regId registration ID
    */
   private void storeRegistrationId(Context context, String regId) {
       final SharedPreferences prefs = getGCMPreferences(context);
       int appVersion = getAppVersion(context);
       Log.i(TAG, "Saving regId on app version " + appVersion);
       SharedPreferences.Editor editor = prefs.edit();
       editor.putString(PROPERTY_REG_ID, regId);
       editor.putInt(PROPERTY_APP_VERSION, appVersion);
       editor.commit();
   }
   
   /**
    * Stores the driver phone number in the application's
    * {@code SharedPreferences}.
    *
    * @param context application's context.
    * @param phoneNumber driver phone number
    */
   private void storePhoneNumber(Context context, String phoneNumber) {
       final SharedPreferences prefs = getGCMPreferences(context);
       int appVersion = getAppVersion(context);
       Log.i(TAG, "Saving regId on app version " + appVersion);
       SharedPreferences.Editor editor = prefs.edit();
       editor.putString(PHONE_NUMBER, phoneNumber);
       editor.commit();
   }

   /**
    * Stores the driver phone number in the application's
    * {@code SharedPreferences}.
    *
    * @param context application's context.
    * @param driverId driver phone number
    */
   private void storeDriverId(Context context, String driverId) {
       final SharedPreferences prefs = getGCMPreferences(context);
       int appVersion = getAppVersion(context);
       Log.i(TAG, "Saving driver on app version " + appVersion);
       SharedPreferences.Editor editor = prefs.edit();
       editor.putString(DRIVER_ID, driverId);
       editor.commit();
   }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(LOCATION_UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(LOCATION_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build();
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        // TODO Auto-generated method stub
        
    }


    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub
        
    }


    @Override
    public void onLocationChanged(Location location) {
        Globals.mCurrentLocation = location;
        new GeoUpdateTask().execute(location);
        Log.i(TAG, "published location lat "+location.getLatitude()+" long "+location.getLongitude());
    }


    public static String getDriverId() {
        return driverId;
    }


    public static void setDriverId(String driverId) {
        HomeActivity.driverId = driverId;
    }


}

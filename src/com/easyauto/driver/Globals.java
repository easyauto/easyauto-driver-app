package com.easyauto.driver;

import android.location.Location;

public class Globals {

    public static Trip currentTrip;
    public static Location mCurrentLocation;
    public static Boolean isAvailable;

    public static Boolean getIsAvailable() {
        return isAvailable;
    }

    public static void setIsAvailable(Boolean isAvailable) {
        Globals.isAvailable = isAvailable;
    }

}


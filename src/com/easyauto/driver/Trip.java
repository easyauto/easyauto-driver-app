package com.easyauto.driver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

public class Trip{

    public static String TRIP_ID = "tripId";
    public static String TRIP_FROM = "tripFrom";
    public static String TRIP_FROM_LAT = "lat";
    public static String TRIP_FROM_LNG = "lng";
    public static String TRIP_TO = "tripTo";
    
    public static String TRIP_DETAILS = "tripDetails";
    public static String FAIL_MSG = "trip_fail";
    public static String SUCCESS_MSG = "trip_success";
    public static enum TRIP_STATES { SUCCESS, FAILURE};
    
    private String fromLocation, id;
    private double fromLat, fromLng;
    private String customerPhoneNum;

    public Trip(String fromLocation, String id, double fromLat, double fromLng) {
        super();
        this.fromLocation = fromLocation;
        this.id = id;
        this.fromLat = fromLat;
        this.fromLng = fromLng;
        
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getFromLat() {
        return fromLat;
    }

    public void setFromLat(double fromLat) {
        this.fromLat = fromLat;
    }

    public double getFromLng() {
        return fromLng;
    }

    public void setFromLng(double fromLng) {
        this.fromLng = fromLng;
    }


}

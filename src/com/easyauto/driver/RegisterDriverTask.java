package com.easyauto.driver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.util.Log;
import android.view.View;

public class RegisterDriverTask extends
        AbstractAsyncTask<String, Integer, String> {

    RegisterDriverTask(Context context) {
        super(context);
    }

    private static String REGISTER_URL = "http://findauto.in/registerDriverApp.php";
    private static String TAG = "EADRegisterDriverTask";

    @Override
    protected String doInBackground(String... phoneNumbers) {
        if(phoneNumbers != null && phoneNumbers[0] != null){
            return registerDriver(phoneNumbers[0]);
        }
        return "";
    }

    private String registerDriver(String phoneNumber) {
        String driverId = "";
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(REGISTER_URL);

        try {
            // TODO: Implement basic authentication
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("phone", phoneNumber));
            
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));  

            // Execute HTTP Post Request
            Log.i(TAG,"Registering driver with phoneNumber as "+phoneNumber);
            HttpResponse response = httpclient.execute(httppost);
            
            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
                if(response.getEntity().getContentLength() != 0){
                    driverId = EntityUtils.toString(response.getEntity());
                    Log.i(TAG,"response is "+driverId);
                    return driverId;
                } else {
                    Log.e(TAG, "register response is empty");
                }
            }
            return driverId;
        } catch (ClientProtocolException e) {
            Log.i(TAG,"ClientProtocolException while registering driver "+e.getMessage());
            return driverId;
        } catch (IOException e) {
            Log.i(TAG,"IOException while registering driver "+e.getMessage());
            return driverId;
        }
    }

}

package com.easyauto.driver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class UpdateGcmIdTask extends AsyncTask<String, Integer, Boolean> {

    private static String ACCEPT_URL = "http://findauto.in/updateDriverGCM.php";
    private static String TAG = "UpdateGcmIdTask";
    
    private Trip trip;

    Context context;

    public UpdateGcmIdTask(Context context){
        this.context = context.getApplicationContext();
    }

    @Override
    protected Boolean doInBackground(String... gcmId) {
        
        if(gcmId != null && gcmId[0] != null){
            return updateGcmId(gcmId[0]);
        }
        return false;
    }

    private Boolean updateGcmId(String gcmId) {

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(ACCEPT_URL);

        try {
            // TODO: Implement basic authentication
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("driver_id", HomeActivity.getDriverId()+""));
            nameValuePairs.add(new BasicNameValuePair("gcm_id", gcmId));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            Log.i(TAG,"Updating GCM ID in server as "+gcmId);
            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            
            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
                //TODO: Check response text for success or failure
                Log.i(TAG,"Updated GCM ID successfully as "+gcmId);
                return true;
            }
            return false;
        } catch (ClientProtocolException e) {
            Log.i(TAG,"ClientProtocolException while updating location "+e.getMessage());
            return false;
        } catch (IOException e) {
            Log.i(TAG,"IOException while updating location "+e.getMessage());
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
    }
    

}
